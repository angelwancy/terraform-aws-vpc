resource "aws_route" "routes" {
    for_each = var.routes

    route_table_id = each.value.route_table_ids_refs
    destination_cidr_block = each.value.destination_cidr_block
    destination_ipv6_cidr_block = each.value.destination_ipv6_cidr_block

    nat_gateway_id = each.value.nat_gateway_id_ref
    vpc_endpoint_id = each.value.vpc_endpoint_id_ref != null ? aws_vpc_endpoint.vpc_endpoints[each.value.vpc_endpoint_id_ref] : null
}