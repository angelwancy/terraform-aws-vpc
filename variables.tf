variable "aws_region" {
  type        = string
  default     = "ap-east-1"
}

variable "subnets" {
  type        = map(any)
  default     = {}
}

variable "security_group_rules" {
  type        = map(any)
  default     = {}
}

variable "security_groups" {
  type        = map(any)
  default     = {}
}

variable "vpc_endpoints" {
  type        = map(any)
  default     = {}
}

variable "vpcs" {
  type        = map(any)
  default     = {}
}

variable "route_tables" {
  type        = map(any)
  default     = {}
}

variable "routes" {
  type        = map(any)
  default     = {}
}

variable "nat_gateways" {
  type        = map(any)
  default     = {}
}