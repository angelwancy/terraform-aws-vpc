# terraform-aws-vpc

Terraform AWS network modules to create network components such as VPC, subnets, route tables, NAT gateways, public IPs and its association.

For details please refer to Hashicorp's AWS Registry.
https://registry.terraform.io/modules/terraform-aws-modules
