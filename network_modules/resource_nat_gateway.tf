resource "aws_nat_gateway" "nat_gateways" {
   for_each = var.nat_gateways

   subnet_id = each.value.subnet_id_ref
   tags = each.value.tags

   depends_on = [
       aws_internet_gateway.internet_gateways
   ]
}