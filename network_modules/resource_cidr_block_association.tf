resource "aws_vpc_cidr_block_association" "vpc_cidr_block_associations" {
    for_each = var.vpc_cidr_block_associations

    vpc_id = each.value.vpc_id_ref[0] : each.value.vpc_endpoint_id
    cidr_block = each.value.cidr_block
}