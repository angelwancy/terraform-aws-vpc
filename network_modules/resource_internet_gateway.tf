resource "aws_internet_gateway" "internet_gateways" {
    for_each = var.internet_gateways

    vpc_id = each.value.vpc_id_ref
    tags = each.value.tags
}