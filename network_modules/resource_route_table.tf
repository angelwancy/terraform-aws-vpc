resource "aws_route_table" "route_tables" {
    for_each = var.route_tables

    vpc_id = var.vpc_id_ref
    tags = each.value.tags
}