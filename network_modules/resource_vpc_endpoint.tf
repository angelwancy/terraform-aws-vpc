resource "aws_vpc_endpoint" "vpc_endpoints" {
    for_each = var.vpc_endpoints

    service_name = each.value.service_name_ref
    vpc_id = each.value.vpc_id_ref
    route_table_ids = length(each.value.route_table_ids_refs)
    subnet_ids = length(each.value.subnet_ids_ref)
    security_group_ids = length(each.value.security_group_ids_refs)
    tags = each.value.tags
    vpc_endpoint_type = each.value.vpc_endpoint_type

}