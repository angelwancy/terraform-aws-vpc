resource "aws_subnet" "subnets" {
    for_each = var.subnets

    availability_zone = join("", [var.aws_region, each.value.availability_zone])
    cidr_block = each.value.cidr_block
    ipv6_cidr_block = each.value.ipv6_cidr_block
    map_public_ip = each.value.map_public_ip
    assign_ipv6_address_on_creation = false
    vpc_id = each.value.vpc_id_ref[0]
    tags = each.value.tags
}